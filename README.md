# TablizAssets

Some canonical assets for Tabliz !

**Remember** to use https://tinypng.com, in order to:

1. Reduce the size of the images
2. Make the PNG images exported by Gimp compatible with "ionic cordova resource"


